set tabstop=4
set shiftwidth=4
set expandtab
set autoindent
set smartindent

colorscheme delek
set t_Co=256
set history=1000
set undolevels=1000